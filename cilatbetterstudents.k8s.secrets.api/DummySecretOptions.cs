﻿namespace cilatbetterstudents.k8s.secrets.api
{
    public class DummySecretOptions
    {
        public const string DummySecret = "DummySecret";
        
        public string ConnectionString { get; set; }
        public string ApiKeyOfSomeSort { get; set; }
    }
}