# Sealed secrets

## Description
A simple aspnet core based API service which exposes two secrets injected.

## How to run

1. Run container image and directs traffic to port 80 internally. E.g. `docker run -it --rm -p 8080:80 cilatbetterstudents/sealed-secrets:1.0.0`
1. Mount "secrets" at `/app/secrets/appsettings.secrets.json` - contents of the file should match example below
1. Navigate to path `/secrets` on service. E.g. `http://localhost:8080/Secrets`.

## Example config
```
{
  "DummySecret": {
    "ConnectionString": "Something secret",
    "ApiKeyOfSomeSort": "This is an api key yes yes"
  }
}
```
