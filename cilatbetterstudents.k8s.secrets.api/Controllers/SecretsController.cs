﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace cilatbetterstudents.k8s.secrets.api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SecretsController : ControllerBase
    {
        private readonly IOptions<DummySecretOptions> _secrets;

        public SecretsController(IOptions<DummySecretOptions> secrets)
        {
            _secrets = secrets;
        }

        [HttpGet]
        public DummySecretOptions Get()
        {
            return _secrets.Value;
        }
    }
}